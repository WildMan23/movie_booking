/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moviebooking;

/**
 *
 * @author Admin
 */
public class Account {
    private String userName;
    private String password;
    private String fname;
    private String lname;
    
    public Account(){}
    public Account(String userName, String password, String fname, String lname){
        setUserName(userName);
        setPassword(password);
        setFname(fname);
        setLname(lname); 
    }
    
    public void setUserName(String userName){
        if(userName.length()>5)
            this.userName=userName;
        else{
            this.userName=fname+"."+lname;
        }
    }
    public void setPassword(String password){
        if(password.length()>5)
            this.password = password;
        else{
            this.password = lname+"1234567";
        }
    }
    
    public void setFname(String fname){
        this.fname=fname;
    }
    
    public void setLname(String lname){
        this.lname=lname;
    }
    
    public String getUserName(){
    return userName;
    }
    public String getPassword(){
    return password;
    }
    public String getFname(){
    return fname;
    }
    public String getLname(){
    return lname;
    }
}
